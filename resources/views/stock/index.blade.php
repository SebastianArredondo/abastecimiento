@extends('layouts.app', ['activePage' => 'productos', 'titlePage' => __('Producto')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Movimientos del Producto') }}</h4>
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('producto') }}" class="btn btn-sm btn-primary">{{ __('Volver') }}</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                    <th>
                          {{ __('Bodega') }}
                      </th>
                      <th>
                          {{ __('Fecha') }}
                      </th>
                      <th>
                        {{ __('Tipo') }}
                      </th>
                      <th>
                        {{ __('Cantidad') }}
                      </th>
                    </thead>
                    <tbody>
                      @foreach($movimientos as $movimiento)
                        <tr>
                        <td>
                            {{ $movimiento->nombre }}
                          </td>
                          <td>
                            {{ $movimiento->created_at }}
                          </td>
                          <td>
                            {{ $movimiento->tipo }}
                          </td>
                          <td>
                            {{ $movimiento->cantidad }}
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection