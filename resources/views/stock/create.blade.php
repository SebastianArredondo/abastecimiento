@extends('layouts.app', ['activePage' => 'stock', 'titlePage' => __('Stock')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('stock.store') }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Mover Stock') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
              @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Bodega') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                      <select class="form-control" name="bodega_id" id="input-bodega_id">
                        @foreach($data['bodegas'] as $bodega)
                          <option value="{{ $bodega->id }}">{{ $bodega->nombre }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Producto') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                    <select class="form-control" name="producto_id" id="input-producto_id">
                        @foreach($data['productos'] as $producto)
                          <option value="{{ $producto->id }}">{{ $producto->codigo.' '.$producto->descripcion }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-tipo">{{ __('Tipo') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group">
                    <select class="form-control" name="tipo" id="input-tipo">
                          <option value="Ingreso">Ingreso</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label" for="input-cantidad">{{ __('Cantidad') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('cantidad') ? ' is-invalid' : '' }}" input type="number" name="cantidad" id="input-cantidad" placeholder="{{ __('0') }}" value="" min="1" step="any" required />
                      @if ($errors->has('cantidad'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('cantidad') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Mover Stock') }}</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection