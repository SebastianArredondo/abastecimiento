<?php

namespace App\Http\Controllers;

use App\Stock;
use App\Movimiento;
use App\Bodega;
use App\Producto;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($producto_id)
    {
        return view('stock.index', [
            'movimientos' => 
            Stock::where('producto_id', $producto_id)
            ->join('bodegas', 'stocks.bodega_id', '=', 'bodegas.id')
            ->join('movimientos', 'stocks.movimiento_id', '=', 'movimientos.id')
            ->paginate(15)
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'productos' => Producto::get(),
            'bodegas' => Bodega::get(),
        ];

        return view('stock.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movimiento = new Movimiento([
            'tipo' => $request->get('tipo'),
        ]);

        $movimiento->save();

        $stock = new Stock([
            'bodega_id' => $request->get('bodega_id'),
            'producto_id' => $request->get('producto_id'),
            'movimiento_id' => $movimiento->id,
            'cantidad' => $request->get('cantidad')
        ]);

        $stock->save();

        return redirect()->route('stock.create')->withStatus(__('Movimiento realizado exitosamente.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }
}
