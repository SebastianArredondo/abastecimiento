<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{

    /**
    * @return \Illuminate\View\View
    */
    public function show(Producto $model){
        return view('producto.index', ['productos' => $model->paginate(15)]);
    }

    /**
    * @return \Illuminate\View\View
    */
    public function index(Producto $model){
        return view('producto.index', ['productos' => $model->paginate(15)]);
    }

    /**
    * @return \Illuminate\View\View
    */
    public function create()
    {
        return view('producto.create');
    }

    /**
     * @param  \App\Producto  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $model = new Producto([
            'codigo' => $request->get('codigo'),
            'descripcion' => $request->get('descripcion'),
            'precio' => $request->get('precio')
        ]);
        $model->save();

        return redirect()->route('producto')->withStatus(__('Producto creado exitosamente.'));
    }

    /**
     * @param  \App\Producto $producto
     * @return \Illuminate\View\View
     */
    public function edit(Producto $producto)
    {
        return view('producto.edit', compact('producto'));
    }

    /**
     * @param  \App\Http\Requests\ProductoRequest  $request
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Producto $producto)
    {
        $producto->update(
            $request->all()
        );

        return redirect()->route('producto')->withStatus(__('Producto modificado exitosamente.'));
    }

    /**
     * @param  \App\Producto  $producto
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Producto $producto)
    {
        $producto->delete();
        return redirect()->route('producto')->withStatus(__('Producto eliminado exitosamente.'));
    }
}
